"""
 Take as input the square torus generated above and "move" the points to the usual embedded torus.
 That is, we leave the other data arrays alone but we replace the output points.
 Notice in particular that we don't adjust the associated topology,
 merely the referenced points.

 This torus will be:
 centered at the origin
 with equatorial plane normal to z
 centerRadius distance from center of torus to center of tube
 and tubeRadius distance from center of tube to surface of torus.

 By longitudinal we mean indices/angles/etc increasing around the z-axis
 and by latitudinal we mean those increasing around the tube
"""

import numpy as np
from vtk.numpy_interface import algorithms as algs
from vtk.numpy_interface import dataset_adapter as da

longRes = inputs[0].FieldData.GetArray("Resolution")[0]
latRes = inputs[0].FieldData.GetArray("Resolution")[1]
tubeRadius = 1.0 # From the center of the tube to the surface
centerRadius = 2.0 # Distance from center of torus to the center of the longitidinal circles

numPts = longRes*latRes
pts = vtk.vtkPoints()

for i in range(longRes):
    dihedralLong = inputs[0].Points[i * latRes,0] # The pts at (i * latRes + j) should all have the same 1st coord
    for j in range(latRes):
        index = (i * latRes) + j
        dihedralLat = inputs[0].Points[index,1]
        x = (centerRadius + tubeRadius * cos(dihedralLat)) * cos(dihedralLong)
        y = (centerRadius + tubeRadius * cos(dihedralLat)) * sin(dihedralLong)
        z = tubeRadius * sin(dihedralLat)
        pts.InsertNextPoint(x,y,z)

output.SetPoints(pts)
