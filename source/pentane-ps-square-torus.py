""" 
    Here we produce a square torus with periodic boundary.
    (We "wrap" the square torus in a separate filter.
    This'll be organized slightly differently when separated from Paraview.
    We'll correlate longitudinal with the x direction and latitudinal with y
"""
import numpy as np
#import scipy.constants as spconst
from vtk.numpy_interface import dataset_adapter as da

"""
    OPLS-aa n-alkane potential energy formula, derived by members of YZ's group. 
    This does not account for the energy between non-bonded carbon atoms.

    Parameters
    ----------
    phi : numpy ndarray
        Mesh of the domain (n copies of [0,pi]).

    Returns
    -------
    V : numpy ndarray
        Function values over the domain.

    """

def potential_aa(phi):
    V1 = 2.9288 - 1.4644*cos(phi - pi) + 0.2092*(cos(phi - pi)**2) - 1.6736*(cos(phi - pi)**3)
    V2 = 0.6276 + 1.8828*cos(phi - pi) - 2.5104*(cos(phi - pi)**3)
    V2a = 0.6276 + 1.8828*cos( -phi - (1/3)*pi) - 2.5104*(cos( -phi - (1/3)*pi)**3)
    V2b = 0.6276 + 1.8828*cos(phi - (1/3)*pi) - 2.5104*(cos(phi - (1/3)*pi)**3) 
    V = V1 + V2 + 3*V2a + 3*V2b
    return V

    """
    OPLS-ua n-alkane potential energy formula, derived by members of YZ's group. 
    Does not account for the energy between non-bonded carbon atoms.

    Parameters
    ----------
    phi : numpy ndarray
        Mesh of the domain (n copies of [0,pi]).

    Returns
    -------
    V : numpy ndarray
        Function values over the domain.

    """

k = 0.008314463 # Boltzmann constant in kJ/(mol K)

def potential_ua(phi):
    c1 = 355.03 * k
    c2 = -68.19 * k
    c3 = 791.32 * k
    V1 = c1 * (1+cos(phi))
    V2 = c2 * (1-cos(2*phi))
    V3 = c3 * (1+cos(3*phi))
    V = V1 + V2 + V3
    return V

 
longRes = 256
latRes = 256
res = [longRes,latRes]
numPts = np.prod(res)

output.FieldData.AddArray(da.numpyTovtkDataArray(res, "Resolution"))

pts = vtk.vtkPoints()

PEField = np.empty(numPts, dtype=np.float64)

numCells = numPts * 2
output.Allocate(numCells, 1)

for i in range(longRes):
    dihX = 2 * pi * i / longRes
    PEX = potential_ua(dihX)
    for j in range(latRes):
        dihY = 2 * pi * j / latRes
        PEY = potential_ua(dihY)
        index = (i * latRes) + j
        pts.InsertNextPoint(dihX,dihY, 0)
        PEField[index] = PEX + PEY
        
        """ 
            We construct two triangular cells, calculating the index of each vertex
            (Note that it is ok that we refer to indices for points which haven't been included yet)
            At each index, we have two triangles to construct:
                The one consisting of points (i,j) , (i+1,j) , and (i,j+1)
                as well as that consisting of (i+1,j) , (i,j+1), and (i+1,j+1)
                where i+1 and j+1 are mod their respective resolutions.
        """ 
        
        ii = i + 1
        jj = j + 1
        if(ii == longRes):
            ii = 0
        if(jj == latRes):
            jj = 0
        
        indexB = (ii * latRes) + j
        indexC = (i * latRes) + jj
        indexD = (ii * latRes) + jj

        triangleA = vtk.vtkTriangle()
        triangleA.GetPointIds().SetId(0,index)
        triangleA.GetPointIds().SetId(1,indexB)
        triangleA.GetPointIds().SetId(2,indexC)
        output.InsertNextCell(triangleA.GetCellType(), triangleA.GetPointIds())

        triangleB = vtk.vtkTriangle()
        triangleB.GetPointIds().SetId(0,indexB)
        triangleB.GetPointIds().SetId(1,indexD)
        triangleB.GetPointIds().SetId(2,indexC)
        output.InsertNextCell(triangleB.GetCellType(), triangleB.GetPointIds())

output.SetPoints(pts)
output.PointData.append(PEField, "V")
