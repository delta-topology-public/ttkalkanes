import numpy as np
from vtk.numpy_interface import algorithms as algs
from vtk.numpy_interface import dataset_adapter as da


"""
    OPLS-aa n-alkane potential energy formula, derived by members of YZ's group. 
    This does not account for the energy between non-bonded carbon atoms.

    Parameters
    ----------
    phi : numpy ndarray
        Mesh of the domain (n copies of [0,pi]).

    Returns
    -------
    V : numpy ndarray
        Function values over the domain.

    """

def potential_aa(phi):
    V1 = 2.9288 - 1.4644*cos(phi - pi) + 0.2092*(cos(phi - pi)**2) - 1.6736*(cos(phi - pi)**3)
    V2 = 0.6276 + 1.8828*cos(phi - pi) - 2.5104*(cos(phi - pi)**3)
    V2a = 0.6276 + 1.8828*cos( -phi - (1/3)*pi) - 2.5104*(cos( -phi - (1/3)*pi)**3)
    V2b = 0.6276 + 1.8828*cos(phi - (1/3)*pi) - 2.5104*(cos(phi - (1/3)*pi)**3) 
    V = V1 + V2 + 3*V2a + 3*V2b
    return V

    """
    OPLS-ua n-alkane potential energy formula, derived by members of YZ's group. 
    Does not account for the energy between non-bonded carbon atoms.

    Parameters
    ----------
    phi : numpy ndarray
        Mesh of the domain (n copies of [0,pi]).

    Returns
    -------
    V : numpy ndarray
        Function values over the domain.

    """

k = 0.008314463 # Boltzmann constant in kJ/(mol K)

def potential_ua(phi):
    c1 = 355.03 * k
    c2 = -68.19 * k
    c3 = 791.32 * k
    V1 = c1 * (1+cos(phi))
    V2 = c2 * (1-cos(2*phi))
    V3 = c3 * (1+cos(3*phi))
    V = V1 + V2 + V3
    return V


d = 3

resX = 64
resY = 64
resZ = 64

res = [resX, resY, resZ]

output.FieldData.AddArray(da.numpyTovtkDataArray(res, "Resolution"))

ind = [None] * d
dih = [None] * d
PE = [None] * d

numPts = np.prod(res)
pts = vtk.vtkPoints()

for i in range(d):
    ind[i] = np.arange(res[i], dtype=np.int32)
    dih[i] = np.empty(res[i], dtype=np.float64)
    dih[i] = (2 * pi / res[i]) * ind[i]

    PE[i] = potential_ua(dih[i])
    #pts[i] = np.empty(numPts, dtype=np.float64)

PEField = np.empty(numPts, dtype=np.float64)
normals = []

numCells = numPts * 5
output.Allocate(numCells, 1)

for i in range(resX):
    for j in range(resY):
        for k in range(resZ):
            index = i * resY * resZ + j * resZ + k
            """pts[0][index] = dih[0][i]
            pts[1][index] = dih[1][j]
            pts[2][index] = dih[2][k]"""
            pts.InsertNextPoint(dih[0][i],dih[1][j],dih[2][k])
            PEField[index] = PE[0][i] + PE[1][j] + PE[2][k]
            normals.append([1,1,1])
            
            
            # We need to create a cell with the vertices +1 in each dim
            # Excepting when we are at the end of an index and need to reference the start
            ii = (i + 1)
            jj = j + 1
            kk = k + 1
            if(ii == resX):
                ii = 0
            if(jj == resY):
                jj = 0
            if(kk == resZ):
                kk = 0
            
            indexB = ii * resY * resZ + j * resZ + k
            indexC = ii * resY * resZ + jj * resZ + k
            indexD = i * resY * resZ + jj * resZ + k
            indexE = i * resY * resZ + j * resZ + kk
            indexF = ii * resY * resZ + j * resZ + kk
            indexG = ii * resY * resZ + jj * resZ + kk
            indexH = i * resY * resZ + jj * resZ + kk

            tetra1 = vtk.vtkTetra()
            tetra1.GetPointIds().SetId(0,index)
            tetra1.GetPointIds().SetId(1,indexB)
            tetra1.GetPointIds().SetId(2,indexD)
            tetra1.GetPointIds().SetId(3,indexE)
            output.InsertNextCell(tetra1.GetCellType(), tetra1.GetPointIds())
            
            tetra2 = vtk.vtkTetra()
            tetra2.GetPointIds().SetId(0,indexB)
            tetra2.GetPointIds().SetId(1,indexD)
            tetra2.GetPointIds().SetId(2,indexE)
            tetra2.GetPointIds().SetId(3,indexG)
            output.InsertNextCell(tetra2.GetCellType(), tetra2.GetPointIds())

            tetra3 = vtk.vtkTetra()
            tetra3.GetPointIds().SetId(0,indexD)
            tetra3.GetPointIds().SetId(1,indexE)
            tetra3.GetPointIds().SetId(2,indexG)
            tetra3.GetPointIds().SetId(3,indexH)
            output.InsertNextCell(tetra3.GetCellType(), tetra3.GetPointIds())

            tetra4 = vtk.vtkTetra()
            tetra4.GetPointIds().SetId(0,indexB)
            tetra4.GetPointIds().SetId(1,indexG)
            tetra4.GetPointIds().SetId(2,indexE)
            tetra4.GetPointIds().SetId(3,indexF)
            output.InsertNextCell(tetra4.GetCellType(), tetra4.GetPointIds())

            tetra5 = vtk.vtkTetra()
            tetra5.GetPointIds().SetId(0,indexB)
            tetra5.GetPointIds().SetId(1,indexD)
            tetra5.GetPointIds().SetId(2,indexG)
            tetra5.GetPointIds().SetId(3,indexC)
            output.InsertNextCell(tetra5.GetCellType(), tetra5.GetPointIds())
    
            """
            hexIndices.append(ii * resY * resZ + j * resZ + k)
            hexIndices.append(ii * resY * resZ + jj * resZ + k)
            hexIndices.append(i * resY * resZ + jj * resZ + k)
            hexIndices.append(i * resY * resZ + j * resZ + kk)
            hexIndices.append(ii * resY * resZ + j * resZ + kk)
            hexIndices.append(ii * resY * resZ + jj * resZ + kk)
            hexIndices.append(i * resY * resZ + jj * resZ + kk)

            hexahedron = vtk.vtkHexahedron()
            for n in range(8):
                hexahedron.GetPointIds().SetId(n,hexIndices[n])

            output.InsertNextCell(hexahedron.GetCellType(), hexahedron.GetPointIds())
            """

#coords = algs.make_vector(pts[0],pts[1],pts[2])
#visPts = vtk.vtkPoints()
#visPts.SetData(da.numpyTovtkDataArray(coords, "Points"))
output.SetPoints(pts)
output.PointData.SetNormals(da.numpyTovtkDataArray(normals, "Normals"))
output.PointData.append(PEField, "V")
