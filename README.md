# TTKAlkanes

Use case of the Topology ToolKit (TTK) for the persistent homology and Morse-Smale complex of the alkane molecules for the NSF DELTA project.

As of 10/23/20, the TTK is utilized through the Paraview front end, and Paraview state files are available in the paraview directory which include the relevant python code already.
The python code constructs the appropriate domain and calculates the energy landscape. The code is given in the source directory as individual snippets for use with Programmable Filters/Sources in Paraview, but the code itself works in the standard VTK pipeline and so will be integrated into a standalone application soon.
