This directory contains Paraview state files which utilize TTK in a VTK Pipeline to calculate the persistent homology and Morse-Smale complex of the nAlkanes.

pentane-analytic.pvsm   A simple layout, showing the energy landscape and Morse-Smale complex on both a donut and square torus